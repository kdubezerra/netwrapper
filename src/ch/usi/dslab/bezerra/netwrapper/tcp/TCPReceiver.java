/*

 Netwrapper - A library for easy networking in Java
 Copyright (C) 2015, University of Lugano
 
 This file is part of Netwrapper.
 
 Netwrapper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

package ch.usi.dslab.bezerra.netwrapper.tcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.usi.dslab.bezerra.netwrapper.Message;

public class TCPReceiver implements Runnable {

   public static class ReceivingSelectorPool {
      Map<ReceivingSelector, Integer> recvSelectorsAndCounters = new ConcurrentHashMap<ReceivingSelector, Integer>();

      ReceivingSelectorPool(BlockingQueue<TCPMessage> destinationQueue) {
         this(destinationQueue, Runtime.getRuntime().availableProcessors());
      }

      ReceivingSelectorPool(BlockingQueue<TCPMessage> destinationQueue, int poolSize) {
         for (int i = 0; i < poolSize; i++) {
            ReceivingSelector rs = new ReceivingSelector(destinationQueue);
            recvSelectorsAndCounters.put(rs, 0);
         }
      }

      public void stopAll() {
         for (ReceivingSelector rs : recvSelectorsAndCounters.keySet())
            rs.stop();
      }

      synchronized void addConnectionToPool(TCPConnection connection) {
         try {
            connection.channel.configureBlocking(false);
         } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
         }

         int lowestChanCount = Integer.MAX_VALUE;
         ReceivingSelector chosenSelectorReceiver = null;
         for (Entry<ReceivingSelector, Integer> entry : recvSelectorsAndCounters.entrySet()) {
            int selChanCount = entry.getValue();
            if (selChanCount < lowestChanCount) {
               lowestChanCount = selChanCount;
               chosenSelectorReceiver = entry.getKey();
            }
         }

         recvSelectorsAndCounters.put(chosenSelectorReceiver, lowestChanCount + 1);
         chosenSelectorReceiver.addConnectionToSelector(connection);
      }
   }

   public static class ReceivingSelector implements Runnable {

      // ==================
      // INSTANCE MEMBERS
      // ==================

      Logger logger = LogManager.getLogger(ReceivingSelector.class.getName());

      Selector recvSelector;
      Thread recvSelectorThread;
      BlockingQueue<TCPMessage> destinationQueue;
      Queue<TCPConnection> pendingConnections = new ConcurrentLinkedQueue<TCPConnection>();
      boolean running = true;

      public ReceivingSelector(BlockingQueue<TCPMessage> destinationQueue) {
         try {
            this.destinationQueue = destinationQueue;
            this.recvSelector = Selector.open();
            this.recvSelectorThread = new Thread(this, "ReceivingSelector");
            this.recvSelectorThread.start();
         } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
         }
      }

      void addConnectionToSelector(TCPConnection connection) {
         pendingConnections.add(connection);
      }

      void stop() {
         running = false;
      }

      @Override
      public void run() {
         try {
            while (running) {

               int readyKeys;
               if (pendingConnections.isEmpty())
                  readyKeys = recvSelector.select(1000);
               else
                  readyKeys = recvSelector.selectNow();
               
               // checking if there is some pending receive channel, while
               // avoiding starvation by
               // registering at most one pending channel to write at a time
               TCPConnection pendingConnection = pendingConnections.poll();
               if (pendingConnection != null)
                  pendingConnection.channel.register(recvSelector, SelectionKey.OP_READ, pendingConnection);
               if (readyKeys == 0)
                  continue;

               Iterator<SelectionKey> i = recvSelector.selectedKeys().iterator();

               while (i.hasNext()) {
                  SelectionKey key = i.next();
                  i.remove();

                  if (!key.isValid())
                     continue;

                  try {
                     if (key.isReadable())
                        processMessage(key);
                  } catch (CancelledKeyException e) {
                     logger.debug("Key cancelled - probably disconnected... Closing channel.");
                     key.channel().close();
                  }
               }
            }
         } catch (IOException e) {

         }
      }

      void processMessage(SelectionKey key) {
         try {
            SocketChannel ch = (SocketChannel) key.channel();
            TCPConnection connection = (TCPConnection) key.attachment();

            int readBytes = ch.read(connection.receiveBuffer);

            if (readBytes == -1) {
               ch.close();
               return;
            }

            connection.receiveBuffer.flip();
            while (hasCompleteMessage(connection)) {
               int length = connection.receiveBuffer.getInt();
               byte[] rawMessage = new byte[length];
               connection.receiveBuffer.get(rawMessage);
               destinationQueue.put(new TCPMessage(connection, Message.createFromBytes(rawMessage)));
            }
            
            connection.receiveBuffer.compact();
         } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
         }
      }

      boolean hasCompleteMessage(TCPConnection connection) {
         int receivedBytes = connection.receiveBuffer.limit() - connection.receiveBuffer.position();

         if (receivedBytes < 4)
            return false;

         int length = connection.receiveBuffer.getInt();
         int totalLength = length + 4;
         connection.receiveBuffer.position(connection.receiveBuffer.position() - 4);
         
         if (connection.receiveBuffer.capacity() < totalLength) {
            int newCapacity = Math.max(connection.receiveBuffer.capacity() * 2, totalLength);
            ByteBuffer biggerBuffer = ByteBuffer.allocate(newCapacity);
            biggerBuffer.put(connection.receiveBuffer);
            biggerBuffer.flip();
            connection.receiveBuffer = biggerBuffer;
            return false;
         }

         if (receivedBytes < 4 + length)
            return false;

         return true;
      }
   }

   Logger logger = LogManager.getLogger(TCPReceiver.class.getName());

   int port;
   BlockingQueue<TCPMessage> receivingQueue;

   Thread TCPReceiverConectionListener = null;
   boolean running = true;

   ReceivingSelectorPool receivingSelectorPool = null;
   Selector selector = null;
   ServerSocketChannel serverSocketChannel = null;
   private static final int CONNECTION_BUFFER_SIZE = 262144;

   public TCPReceiver() {
      this(-1);
   }
   
   public TCPReceiver(int port) {
      try {
         this.receivingQueue = new LinkedBlockingQueue<TCPMessage>();
         this.receivingSelectorPool = new ReceivingSelectorPool(this.receivingQueue);
         this.port = port;
         if (port >= 0) {
            this.selector = Selector.open();
            TCPReceiverConectionListener = new Thread(this, "TCPReceiver Connection Listener");
            TCPReceiverConectionListener.start();
         }
      } catch (IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }

   public void stop() {
      receivingSelectorPool.stopAll();
      running = false;
   }

   @Override
   public void run() {
      try {

         serverSocketChannel = ServerSocketChannel.open();
         serverSocketChannel.socket().bind(new InetSocketAddress(port));
         serverSocketChannel.configureBlocking(false);
         serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

         while (running) {

            int readyKeys = selector.select(1000);
            if (readyKeys == 0)
               continue;

            Iterator<SelectionKey> i = selector.selectedKeys().iterator();

            while (i.hasNext()) {
               SelectionKey key = i.next();
               i.remove();

               if (!key.isValid())
                  continue;

               try {
                  if (key.isAcceptable())
                     acceptConnection(key);
               } catch (CancelledKeyException e) {
                  logger.debug("Key cancelled - probably disconnected... Closing channel.");
                  key.channel().close();
               }
            }

         }

         logger.debug("Exiting tcpreceiver's select loop");
      } catch (IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }

   private void acceptConnection(SelectionKey key) {
      try {
         ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
         SocketChannel connectionChannel = serverSocketChannel.accept();
         TCPConnection newConnection = new TCPConnection(connectionChannel, CONNECTION_BUFFER_SIZE);
         receivingSelectorPool.addConnectionToPool(newConnection);
      } catch (IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }

   public void addConnection(TCPConnection connection) {
      receivingSelectorPool.addConnectionToPool(connection);
   }

   public TCPMessage receive() {
      try {
         return receivingQueue.take();
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
      return null;
   }

   public TCPMessage receive(long timeout) {
      try {
         return receivingQueue.poll(timeout, TimeUnit.MILLISECONDS);
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
      return null;
   }

}
