/*

 Netwrapper - A library for easy networking in Java
 Copyright (C) 2015, University of Lugano
 
 This file is part of Netwrapper.
 
 Netwrapper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

package ch.usi.dslab.bezerra.netwrapper.codecs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.esotericsoftware.kryo.io.ByteBufferInputStream;

@SuppressWarnings("rawtypes")
public class CodecUncompressedJDK implements Codec {
   
   private static Map<Integer, Class> classesToRegister = new ConcurrentHashMap<Integer, Class>();
   
   public static void registerClassInSerializationIndex(Class c, int id) {
      classesToRegister.put(id, c);
   }
   
   @Override
   public byte[] getBytes(Object obj) {
      byte[] bytes = null;
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      ObjectOutput out = null;
      try {
        out = new ObjectOutputStream(bos);   
        out.writeObject(obj);
        bytes = bos.toByteArray();
      }
      catch (IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
      finally {
        try {
          if (out != null) {
            out.close();
          }
        } catch (IOException ex) {
          // ignore close exception
        }
        try {
          bos.close();
        } catch (IOException ex) {
          // ignore close exception
        }
      }
      return bytes;
   }

   @Override
   public ByteBuffer getByteBufferWithLengthHeader(Object obj) {
      byte[] objectArray = getBytes(obj);
      int byteBufferLength = 4 + objectArray.length;
      ByteBuffer extByteBuffer = ByteBuffer.allocate(byteBufferLength);
      extByteBuffer.putInt(byteBufferLength - 4); // length in first header doesn't
                                        // include that header's length
      extByteBuffer.put(objectArray);
      extByteBuffer.flip();
      return extByteBuffer;
   }

   @Override
   public Object createObjectFromBytes(byte[] bytes) {
      Object obj = null;
      ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
      ObjectInput in = null;
      try {
        in = new ObjectInputStream(bis);
        obj = in.readObject();
      }
      catch (IOException | ClassNotFoundException e) {
         e.printStackTrace();
         System.exit(1);
      } finally {
        try {
          bis.close();
        } catch (IOException ex) {
          // ignore close exception
        }
        try {
          if (in != null) {
            in.close();
          }
        } catch (IOException ex) {
          // ignore close exception
        }
      }
      return obj;
   }

   @Override
   public Object createObjectFromByteBufferWithLengthHeader(ByteBuffer buffer) {
      Object obj = null;
      ObjectInput in = null;
      
      // skip the buffer length parameter
      buffer.position(buffer.position() + 4);
      try {
         in = new ObjectInputStream(new ByteBufferInputStream(buffer));
         obj = in.readObject();
      }
      catch (IOException | ClassNotFoundException e) {
         e.printStackTrace();
         System.exit(1);
      }
      finally {
         try {
            in.close();
         } catch (IOException e) {
            // ignore close exception
         }
      }
      return obj;
   }

   @Override
   public Object deepDuplicate(Object o) {
      byte[] objBytes = getBytes(o);
      return createObjectFromBytes(objBytes);
   }

}
