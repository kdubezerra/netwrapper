package ch.usi.dslab.bezerra.netwrapper.codecs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class CodecGzip implements Codec {

   @Override
   public byte[] getBytes(Object obj) {
      byte[] bytes = null;
      try {
         ByteArrayOutputStream baos = new ByteArrayOutputStream();
         GZIPOutputStream gzipOut = new GZIPOutputStream(baos);
         ObjectOutputStream objectOut = new ObjectOutputStream(gzipOut);
         objectOut.writeObject(obj);
         objectOut.close();
         bytes = baos.toByteArray();
      } catch (IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
      return bytes;
   }

   @Override
   public Object createObjectFromBytes(byte[] bytes) {
      Object obj = null;
      try {
         ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
         GZIPInputStream gzipIn = new GZIPInputStream(bais);
         ObjectInputStream objectIn = new ObjectInputStream(gzipIn);
         obj = objectIn.readObject();
         objectIn.close();
      }
      catch (ClassNotFoundException | IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
      return obj;
   }

   @Override
   public ByteBuffer getByteBufferWithLengthHeader(Object m) {
      // TODO Implement
      return null;
   }

   @Override
   public Object createObjectFromByteBufferWithLengthHeader(ByteBuffer buffer) {
      // TODO Implement
      return null;
   }

   @Override
   public Object deepDuplicate(Object o) {
      // TODO Implement
      return null;
   }

}
