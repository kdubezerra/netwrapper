package ch.usi.dslab.bezerra.netwrapper.examples;

import java.io.IOException;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPConnection;

public class SimpleTcpClient {

   public static void main(String[] args) throws InterruptedException, IOException {
      Message message = new Message(new String("message from client"));
      System.out.println("Message class name: " + Message.class.getName());
      TCPConnection serverConnection = new TCPConnection("localhost", 50000);
      serverConnection.sendBusyWait(message);

      Message rawReply = serverConnection.receiveBusyWait();
      String reply = (String) rawReply.getItem(0);
      System.out.println("Reply received: " + reply);
   }


}
